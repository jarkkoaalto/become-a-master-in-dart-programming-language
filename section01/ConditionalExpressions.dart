/*
Confitional Expressions in Dart

condition ? expr1 : expr2

 */
void main(){
  var a = 1000;
  var res = a > 800 ? "Value grater than 800":"Value lesser than or equal to 800";
  print(res);

  var t = null;
  var y = 1000;
  var res1 = t ?? y; // return second option if first is null
  print(res1);
}
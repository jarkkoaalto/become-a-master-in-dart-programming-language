/*
      Switch statement in Dart
      Rules:
 */

void main(){
  var grade = "B";
  switch(grade){
    case "A":
      {
        print("Exellent");
        break;
      }
    case "B": {
      print("Good");
      break;
    }
    case "C": {
      print("Fair");
      break;
    }
    case "D": {
      print("Poor");
      break;
    }
    default: {
      print("Invalid choise");
      break;
    }
  }
}


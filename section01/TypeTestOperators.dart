/*
  Type test operators are used for checking types at runtime.const
  operatoes are:
  is -> true if the object has the specified typed
  !is -> False if the object has the specified type

  Bitwise operators
  -----------------
  and   -> &
  or    -> |
  xor   -> ^
  not   -> ~

  left sift     -> <<
  right sift    -> >>

 */
void main() {
    int n = 200;
    print(n is int);

    double m = 600.32;
    var num = m is! int;
    print(num);


   var a = 2; // bit presentation 10
   var b = 3; // bit presentation 11

    var result = (a&b);
    print("(a & b) => ${result}");
    result = (a|b);
    print("(a|b) => ${result}");
    result = (a^b);
    print("(a^b) => ${result}");
    result = (~b);
    print("(~b) => ${result}");
    result = (a<<b);
    print("(a << b) => ${result}");
    result = (a>>b);
    print("(a >> b) => ${result}");
}
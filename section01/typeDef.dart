/*
  Typedefss can also passed as a parametre to a function
 */

typedef ManyOperation(int firstNo, int secondNo);
  Add(int firstNo, int secondNo){
    print("Add result is ${firstNo + secondNo}");
  }
  Substract(int firstNo, int secondNo){
    print("Substracr result is ${firstNo-secondNo}");
  }
  Divide(int firstNo, int secondNo){
    print("Divide result is ${firstNo/secondNo}");
  }
  Calculator(int a, int b, ManyOperation oper){
    print("Inside calculator");
    oper(a,b);
  }
  main(){
    Calculator(5, 5, Add);
    Calculator(5, 5, Substract);
    Calculator(5, 5, Divide);
  }
/*
Properites of map class
- keys
- values
- length
- isEmpty
- isNotEmpty

 */

void main(){
 var details = {'Username': 'Bill', 'Password':'password123'};
 print(details.keys);
 print(details.values);
 print(details.length);
 print(details.isEmpty);
 print(details.isNotEmpty);
print("*****");
 var guests = {};
 print(guests.isEmpty);
 print(guests.isNotEmpty);
}
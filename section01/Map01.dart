
/*
  MAPS
  Enclose the key-value pairs with a pair of curly brackets
 */
void main(){
  var details = {'Username':'Glenn', 'Password':'secret09'};
  details['UserID'] ="UooooU"; // Add value pair in MAP
  print(details);

  details['Username1'] = 'adonemore';
  details['Username2'] = 'addanotheralso';
  print(details);
}
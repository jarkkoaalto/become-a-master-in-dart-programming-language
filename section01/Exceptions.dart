
main (){
  int x = 12;
  int y = 0;
  int res;

  try{
    res = x ~/ y;
  }
  /*
    Try - On
    on IntegerDivisionByZeroException{
        print("Cannot divide by zero");
    }
  */
  catch(e){
    print(e);
  }
  finally{
    print("Program end");
  }
}


void main(){

  String str1 = 'This is a single line string';
  String str2 = "This is a single line string";
  String str3 = """ thsi is 
  a multiline string """;
  String str4 = ''' Thsi is 
  a multiline string''';

  print(str1);
  print(str2);
  print(str3);
  print(str4);

  /*
   Concatenation or interpolation: + is a used to concatenate / interpolate string
   */
  String sana = "This is a ";
  String sana1 = "school";
  String lause = sana + sana1;
  print("The concatenate the two string ${lause}");

    /*
    ${} can be used to interpolate the value of Dart expression with strings.
     */
    int n = 20+20;
    String str5 = "The sum of 20 and 20 is ${n}";
    String str6 = "The sum of 10 and 10 is ${10+10}";
    print(str5);
    print(str6);

}
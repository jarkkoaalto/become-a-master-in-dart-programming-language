void main() {
  List I = [100,200,300];
  I.add(400);
  print(I); // [100, 200, 300, 400]

  I.add([500,600]);
  print(I);

  I.insert(0,600);
  print(I);

  I.insertAll(0,[1000,2000]);
  print(I);

}
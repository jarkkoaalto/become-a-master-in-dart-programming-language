/*
 operators in Dart
 - Expression: evaluates to a value
 - Operands - Represents the data
 - Operator - Defines how the operands will be processed to produce a value

 - Arithmetic Operators
 - Equality and relational operators
 - Type test operators
 - Bitwise operators
 - Assignment operators
 - Logical operators
 */
void main(){
  var num1 = 1000;
  var num2 = 20;
  var res = 0;

  res = num1 + num2;
  print("Addition: ${res}");

  res = num1 - num2;
  print("Substraction: ${res}");

  res = num1 * num2;
  print("Multiplication: ${res}");

  print("Division: ${num1/num2}");
  res = num1~/num2;
  print("Division returning integer: ${res}");

  res = num1 % num2;
  print("Remainder: ${res}");

  num1++;
  print("Increment: ${num1}");

  num2 --;
  print("Decrement : ${num2}");
}
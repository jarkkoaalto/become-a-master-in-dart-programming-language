void main(){
  String str = "Welcome";
  print(str.codeUnits); // [87, 101, 108, 99, 111, 109, 101]

  print(str.isEmpty); // false
  print("The length of the string is: ${str.length}"); // 7
}
void main(){
  var obj = new Square();
  obj.cal_area();
}

class Shape{
  void cal_area(){
    print("Calling calc area defined in the Shape class");
  }
}

class Ellipse extends Shape{}
class Square extends Shape{}
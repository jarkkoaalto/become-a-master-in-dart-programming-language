/*
  Confitional constructor in Dart
  Statement:

  else ... if ladder

  switch ... case statement

 */

void main(){
  var num = 2;
  if(num > 0){
    print("Number is postive");
  }

  var num2 = 114;
  if(num2 % 2 == 0){
    print("Even");
  }else{
    print("Odd");
  }

  var num3 = 9;
  if(num3 > 0){
    print("${num3} is positive");
  }else if(num3 < 0){
    print("${num3} is negative");
  }else {
    print("${num3} is neither positive nor negative");
  }
}
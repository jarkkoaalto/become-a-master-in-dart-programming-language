/*
 > greater than
 < lesser than
 >= grater than or equal to
 <= lesser than or equal to
 == equality
 != not equal
 */
void main(){
  var num1 = 1000;
  var num2 = 100;

  var res = num1 > num2;
  print("num1 greater than num2 :: " + res.toString());

  res = num1 < num2;
  print("num1 lasser than num2 :: " + res.toString());

  res = num1 >= num2;
  print("num1 grater than or equal to num2 :: " + res.toString());

  res = num1 <= num2;
  print("num1 lesser than or equal to num2 :: " + res.toString());

  res = num1 != num2;
  print("num1 not equal to num2 :: " + res.toString());

  res = num1 == num2;
  print("num1 equal to num2 :: " + res.toString());
}
/*
  Functions of Map class in the dart_core package
  - addAll()
  - clear()
  - remove()
  - forEarch()
 */
void main(){
  Map m = {'name':'Ram', "id": 'E10001'};
  print("Map : ${m}");

  m.addAll({'dept':'Finance','email':"ram@aolq.com"});
  print("Map arter adding is: ${m}");

  m.clear();
  print("Map after inovking clear(): ${m}");

  Map n = {'name':'Ram', "id": 'E10001'};
  dynamic res = n.remove('name');
  print("Value popped from the Map : ${res}");

  var usrMap = {"Name":"Bill", "Email":"Bill@qwe.com"};
  usrMap.forEach((k,v) => print("${k}: ${v}"));
}
void main(){
  var lst = new List();
  lst.add(0);
  lst.add(100);
  lst.add(200);
  lst.add(300);

  print("The first element of the list is: ${lst.first}"); // 0
  print(lst.isEmpty); // false
  print(lst.isNotEmpty); // true
}
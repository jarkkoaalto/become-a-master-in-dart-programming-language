import 'dart:collection';

void main(){
  List<String> logTypes = new List<String>();
  logTypes.add("WARNING");
  logTypes.add("ERROR");
  logTypes.add("INFO");


  for(String type in logTypes){
    print(type);
  }

  Set <int>numberSet = new Set<int>();
  numberSet.add(111);
  numberSet.add(222);
  numberSet.add(333);

  print("Default implementation : ${numberSet.runtimeType}");

  for(var no in numberSet){
    print(no);
  }

  Queue<int> queue = new Queue<int>();
  print("Default implementation: ${queue.runtimeType}");
  queue.addLast(100);
  queue.addLast(200);
  queue.addLast(300);

  for(int no in queue){
    print(no);
  }
}
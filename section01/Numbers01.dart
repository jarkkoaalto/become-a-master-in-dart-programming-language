/*
  Numbers
  Parsing: the parse() static function allows parsing a string
  containing numbering literal into a number

 */
void main(){
  print(num.parse('125'));
  print(num.parse('425.85'));

  print(num.parse('150AB')); // cause exception
  print(num.parse('BBCC'));
}
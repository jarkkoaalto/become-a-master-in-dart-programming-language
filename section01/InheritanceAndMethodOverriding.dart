import 'dart:io';

void main(){
  Child c = new Child();
  c.m1(255);

  StaticMem.num = 456;
  StaticMem.disp();
}


class Parent{
  void m1(int a){print("Value of a ${a}");}
}

class Child extends Parent{
  @override
  void m1(int b){
    print("Value of b ${b}");
  }
}

class StaticMem{
  static int num;
  static disp(){
    print("The value of num is ${StaticMem.num}");
  }
}
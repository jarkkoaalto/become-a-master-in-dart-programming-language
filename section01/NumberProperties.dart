void main(){
  int n = 12000;
  print(n.hashCode); // return value

  int m = 25550;
  print(m.isFinite); // true

  int q = 1654;
  print(q.isInfinite); // false

 int posnumber = 65;
 int negnumber = -35;

 print(posnumber.isNegative); // false
 print(negnumber.isNegative); // true


 int valzero = 0;
 print(posnumber.sign); // 1
 print(negnumber.sign); // -1
 print(valzero.sign); // 0

 print(posnumber.isEven); // 65 = false
 print(posnumber.isOdd); // 65 = true
}
/*
String.codeUnitAt(int index) Function - Code units is a string can be accessed
throgh their indexes.

Returns the 16-bit utf-16 code unit at the given index.
Unic
 */
import 'dart:core';
void main(){
  function_unicode();
}

function_unicode(){
  String x = 'Runes';
  print(x.codeUnitAt(2)); // 110

  print(x.codeUnits); // [82, 117, 110, 101, 115]
  print("\n******************\n");

  "A string".runes.forEach((int rune){
    var character = new String.fromCharCode(rune);
    print(character);
  });
  print("\n******************\n");
  Runes input = new Runes("\u{1f605}");
  print(new String.fromCharCodes(input)); // 😅

}
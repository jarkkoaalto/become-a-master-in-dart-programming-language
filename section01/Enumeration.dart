void main(){
  print(Status.values);
  Status.values.forEach((v) => print("value: $v, index:${v.index}"));
  print("running: ${Status.running}, ${Status.running.index}");
  print("running index: ${Status.values[1]}");
  print("halted index : ${Status.values[4]}");
}

enum Status{
  none,
  running,
  stopped,
  paused,
  halted
}
void main(){

/*
  CompareTo Method returns an integer indicating the relationship
  between the two numbers
  */
  var a = 1.5;
  print(a.compareTo(12));
  print(a.compareTo(1.5));
  print(a.compareTo(0));

  /*
  Returns the larget integer less than or equal to a number-Number.floor()
   */
  var b = 35.65;
  print("The floor value of 35.65 = ${b.floor()}");

  /*
  Return the remainder of a division:
  Number.remainder(x)
   */
  var c = 25;
  var n = 36;
  var m = 15;
  var s = 29;
  print(c.remainder(3));
  print(n.remainder(6));
  print(m.remainder(6));
  print(s.remainder(3));

  /*
   Rounds returns the value of a number rounded to the nearesr integer: Number.round()
   */
  double n1 = 15.24;
  double n2 = 89.67;
  var value = n1.round();
  print(value);

  value = n2.round();
  print(value);

  /*
  toDouble returns the doubel representation of the number's value
   */
  int m1 = 456;
  var value1 = m1.toDouble();
  print("Output = ${value1}");

  /*
  toInt
   */
  double a1= 15.0;
  var valu = a1.toInt();
  print("Output = ${valu}");

  /*
  toString
   */
  int q = 35;
  var valu2 = q.toString();
  print("Output = ${valu2}");

  /*
  Truncate()
   */
  double t1 = 125.76655;
  var arvo = t1.truncate();
  print("The truncated value of 125.76655 = ${arvo}");
}
/*
Class Constructors in DART
- Initializes the variables of the class
- Defines name same as than of the class
- A function and hence can be parameterized
- Cannot have a return type
- A default no-argument constructor is provided

// Constructor body
Class_name(parameter_list
{
}
 */
void main(){
  Car c = new Car('E2019');

  Auto a1 = new Auto.namedConst("Engine 2019");
  Auto a2 = new Auto();

  Truck t = new Truck("Big Diesel 2019");
}

class Car{
  Car(String engine){
    print(engine);
  }
}

class Auto{
  Auto(){
    print("Non-parameterized constructor invoke");
  }
  Auto.namedConst(String engine){
    print("The engine is: ${engine}");
}
}

class Truck{
  String engine;
  Truck(String engine){
    this.engine = engine;
    print("The engine is: ${engine}");
  }
}

/*
  Number porperties and methods in DART

  abs           ->      Returns the absolite value of the number
  ceil          ->      Returns the least integer no smaller that the number
  compareTo     ->      Compares this to other number
  Floor         ->      Returns the greatest integer bot greater than the
                        current number.
  remainder     ->      Returns the truncated remainder after dividing the two number
  Round         ->      Returns the integer closest to the current numbers
  toDouble      ->      Returns the double quicalent of the number.const
  toInt         ->      Returns the integer equivalent fo the number
  toString      ->      Returns the string equivalent representation of the numbers
  truncate      ->      Returnd an integer after discarding any fractional digits
   */

void main(){
  var a = -585;
  var b = 12.24;

  print(a.abs());
  print("The ceiling value of 12.24 = ${b.ceil()}");
}
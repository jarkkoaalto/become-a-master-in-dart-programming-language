enum Color {
  Red,
  Green,
  Blue,
  Yellow
}

void main(){
  print(Color.values);
  Color.values.forEach((v) => print("value: $v index: ${v.index}"));
  print('Green: ${Color.Green}, ${Color.Green.index}');
  print("Blue index: ${Color.values[2]}");
  print("Yellow index: ${Color.values[3]}");

}
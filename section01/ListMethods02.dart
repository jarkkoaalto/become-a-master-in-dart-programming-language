void main(){
  var lst = new List();

  lst.add(10);
  lst.add(100);
  lst.add(200);
  lst.add(300);
  lst.add(400);

  print("The length of the list is: ${lst.last}"); // 400
  print("The list values in reverse is: ${lst.reversed}"); //  (400, 300, 200, 100, 10)

}
/*
  Loop control statements in DART

  control

   break statement        -> using break in a loop causes the program to
                           exit the loop.

   Continue statement     -> Statement skips the subsequent statements in the
                          current iteration and  takes the control back
                          to the beginning of the loop.const

 */
void main(){
  outerloop: // This is the label name
  for(var i= 0; i<4;i++){
    print("innerloop ${i}");
    innerloop:
      for(var j=0; j<4;j++){
        if(j<4) break;

        //Quit the innermost loop
        if(i==1) break innerloop;

        // Do the same thing
        if(i==3) break outerloop;

        // Quit the outerloop
        print("innerloop: ${i}");
      }
  }
}
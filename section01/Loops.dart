/*
 Loop
 */

void main(){

  /*
      For loops
   */
    var num = 6;
    var factorial = 1;

  for(var i = num; i >= 1; i--){
    factorial *= i;
  }
  print(factorial);

  /*
    Definite loop
   */
    var obj =[1000, 2000, 3000];
  for(var prop in obj){
    print(prop);
  }
  /*
  Indefined
   */
    var numb = 5;
    var fac = 1;

  while(numb >= 1){
    fac = fac * numb;
    numb--;
  }
    print("The factorial is ${fac}");
}
void main(){
 String uStr = "NEWYORK";
 String IStr ="Michigan";


 print(uStr.toLowerCase());
 print(IStr.toUpperCase());

 String str1 = "You are ";
 String str2 = "welcome";
 print(str1.toLowerCase());
 print(str2.toUpperCase());

 String tr1 = "   hello   ";
 String tr2 = "  hello            ";
 String tr3 = "   hello";
 print(tr1.trim());
 print(tr2.trim());
 print(tr3.trim());

/*
 compareTo(String other)
 */
String str5 = "B";
String str6 = "B";
String str7 = "C";
String str8 = "Today,is,Friday"; // Split substring

print("str5.compareTo(str6) ${str5.compareTo(str6)}"); // 0
print("str5.compareTo(str7) ${str5.compareTo(str7)}"); // -1
print("str6.compareTo(str7) ${str6.compareTo(str7)}"); // -1
print("New String ${str8.split(',')}");

}
/*
Assignment operators
 =    -> assigns values from the right side operand to the left side operand
       EX: C = A + B, will assing  the value of A + B into CastError
 ??=  -> Assign the value only if the variable is null

 +=   -> It adds the right operand to the left operand and assigns the result
         to the left operands. EX: C += A is equivalent to C = C+A

 -=   -> Substracts the right operand from the left operand and assigns the
         result to the left operand. Ex C -= A is equivalent to C = C-A

 *=   -> Multiplies the right operand with the left operand and assigns the resulr
         to the left operand. EX: C *= A is equivalent to C = C * A

 /=   -> Divides the left operands with the right operand and assigns the
         result to the left operand.

Logical operators:
&&    -> the operator returns true only if all the expressions specified return true
||    -> Or - The operator returns true if at least one of the expressions specific
         return true

 */
void main() {
  var a = 200;
  var b = 10;

  a += b;
  print("a+=b : ${a}");

  a = 400;
  b = 300;
  a -= b;
  print("a-=b: ${a}");

  a = 40;
  b = 30;
  a *= b;
  print("a*=b : ${a}");

  a = 50;
  b = 60;
  a %= b;
  print("a%=b: ${a}");


  var q = 1000;
  var w = 2000;
  var res = ((a<b) && (b > 110));
  print(res);

  res = ((a>b)||(b>9999));
  print(res);
  var res1 = !(a==b);
  print(res1);
}